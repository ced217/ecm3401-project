Submission file containing deliverables for ecm3401-project:  Emergence	and	evolution	of	community	structure	in	Rock	music

This readme file documents the deliverables and source code for the project

Objectives: Using the Band-to-Band website (bandtoband.com), the aim is to gain an understanding of the
evolution of the network of collaborations of Rock musicians.


---

##graphDB

Contains
The scripts required parse the band to band site map allowing the program to parse html files and collect the data
The Node folder which hold the source code for the implementation of various nodes in thes in the network and the folder webdata which holds the data on the xml sitemaps.
The scripts required to load the collected data, as nodes, relationships and properties.
The now deprecated bolt driver which was used to load the ndoes and relationships and some of the properties into the datbase.

---

## Export
Contains
The cypher source required to export the graph as a CSV file for submission.
The graph exported as csv files storing data on; the band, musician and album nodes, their properties and  relationships.

---
---

## Queries
Contains
The scripts used for querying the network using the cypher transactional http endpoints.
The graphs are plotted using plotly a graph visualition library for python.

---
##Rsults
Contains a csv file with the tabular results of the analysis are well as the graph visualised using plotly.




from neo4j.v1 import GraphDatabase




class Relationship:

	nodeA = None
	nodeB = None
	driver = None

	def __init__(self,d):
		self.driver = d

	#def __init__(self,)
	#functions are abstractions, session run actually passes parameter 
	#musician Recorded album 
	#matching based on keys rather than relatioships, allow ensures no duplicate relationships or 
	#duplicate relationships on 
	def recorded(self, tx, musician,album):
		statement = "MATCH (m:Musician {name: {musician_name}}),(a:Album {name: {album_name}})" + "MERGE (m)-[r:Recorded {name: m.fullName + '<->' + a.title, year: a.year }]->(a)"
		tx.run(statement , musician_name= musician, album_name= album)
			

	#album Recorded by musician 
	def recordedBy(self,tx, album,musician):
		statement = "MATCH (a:Album {name: {album_name}}),(m:Musician {name: {musician_name}})" + "MERGE (a)-[r:RecordedBy {name: m.fullName + '<->' + a.title, year: a.year}]->(m)"
		tx.run(statement, album_name=album , musician_name=musician)
			
	
	#Path musician to musician
	def recordedWith(self,tx):			#no need for auxilliary nodes due to pattern matching finding the relevant relationships only then matching on them
		statement = "MATCH (m:Musician)-[p:Recorded]->(a:Album), (n:Musician)-[q:Recorded]->(b:Album) "+ "WHERE a.name = b.name AND NOT m.name = n.name " + "MERGE (m)-[r:RecordedWith {name: m.fullName + '<-' + a.title + '->' + n.fullName, album: a.title, year: a.year }]->(n)"
		tx.run(statement)




	#musician performed in band 
	def performedIn(self, tx, musician,band):
		statement = "MATCH (m:Musician {name: {musician_name}}),(b:Band {name: {band_name}})" + "MERGE (m)-[r:PerformedIn {name: m.fullName + '<->' + b.bandName}]->(b)"
		tx.run(statement, musician_name=musician, band_name=band)
			

	#band had member musician 
	def hadMember(self, tx, band,musician):
		statement = "MATCH (b:Band {name: {band_name}}),(m:Musician {name: {musician_name}})" + "MERGE (b)-[r:HadMember {name: b.bandName  + '<->' + m.fullName} ]->(m)"
		tx.run(statement, band_name=band, musician_name=musician)
			
	'''
	#path band collaborated with band
	def collaboratedWith(self, tx):
		statement = "MATCH (m:Musician)-[:PerformedIn]->(b:Band),(n:Musician)-[:PerformedIn]->(c:Band),(m:Musician)-[:Recorded]->(a:Album), (n:Musician)-[:Recorded]->(z:Album)" +" WHERE a.name = z.name AND NOT b.name = c.name" +" MERGE (b)-[r:HaveCollaborated {name: b.bandName + '<->' + c.bandName, year: a.year , album: a.title} ] ->(c)" 
		#Dont remove data from the database but only match on the album
		#In addition to the weights,
		#Should have attribute of weight, should also have 
		#Don't worry about the musiacians match on just the bands and albums 
		tx.run(statement)
		print("")
	
		#m and n might be the same if some the member played in both bands,
		#if not two members from one band came together for a second band

		#Ignore the Band members given by the database

	#Potential relationship missing two bands having collaborated is there is a single shared musician between them.
	'''


	def query(self,tx):
		statement  = "MATCH (m:Musician)-[:PerformedIn]->(b:Band),(n:Musician)-[:PerformedIn]->(c:Band),(m:Musician)-[:Recorded]->(a:Album), (n:Musician)-[:Recorded]->(z:Album)" 
		statement_ = " WHERE a.name = z.name AND NOT b.name = c.name"
		statementR = " RETURN z.title, z.year, b.name ,c.name "

		statement = statement + statement_ + statementR

		result = tx.run(statement)
		return result
	
	def create(self,tx,bandb,bandc,year,title):
		statement 	= "MATCH (b:Band {name: {bandb}}),(c:Band {name: {bandc}})"
		statement_ 	= "MERGE (b)-[r:HaveCollaborated {name: b.bandName + '<->' + c.bandName, year: {year} , album: {title}} ] ->(c)" 
		
		statement = statement + statement_
		tx.run(statement, bandb=bandb, bandc=bandc, year=year, title=title)
		print("created relationship: collaborated with")

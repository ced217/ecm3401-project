
 
#from dynamic import Dynamic
import requests
import json 
import time


#use bin\neo4j start to run server

'''
This is where the run all will most likely be stored and 
where the driver object instance will be instanciated
and the db loading functions for the scraped data
'''

#db loading functions if necessary will pass data from the webscraper to the neo4j create functions

#initialistion 

def countRows(res):
	text = res.text
	n = text.count("row")
	return n

def matchBand():
	
	limit = "30"
	
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	
	statement1 = "MATCH (b:Band )--(m:Musician)--(a:Album)"   
	statement2 = " Where NOT EXISTS(b.year)"
	statement3 = " Return b.name AS url, min(a.year) AS year Limit "+limit
	
	statements = statement1 + statement2 + statement3 
	data ={	
		"statements" : [ {
    		"statement" : statements
    		
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)

	#print(res.status_code)
	


	r = res.json()
	
	result =  r['results'][0]['data']
	l = int(limit)
	
	return (l,result)
	
	

def setBand(url,year):
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (b:Band {name: {url}})"
	statement2 = " SET b.year = {year}"
	statement3 = "RETURN b.name,b.year"
	statements = statement1+statement2+statement3
	
	
	data ={	
		"statements" : [ {
    		"statement" : statements,
    		"parameters" : {
      			
        			"url" : url,
        			"year" : year
      			
    		}
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(res.status_code)
	r = res.json()
	result =  r['results'][0]['data']
	url = result[0]['row'][0]
	year = result[0]['row'][1]
	print(url,year)
	#print(result)

	
def matchMus():
	limit = "30"
	
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	
	statement1 = "MATCH (m:Musician)--(a:Album)"   
	statement2 = " Where NOT EXISTS(m.year)"
	statement3 = " Return m.name AS url, min(a.year) AS year Limit "+limit
	
	statements = statement1 + statement2 + statement3 
	data ={	
		"statements" : [ {
    		"statement" : statements
    		
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)

	#print(res.status_code)
	
	

	r = res.json()
	
	result =  r['results'][0]['data']
	
	return (int(limit),result)

def setMus(url,year):
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (m:Musician {name: {url}})"
	statement2 = " SET m.year = {year}"
	statement3 = "RETURN m.name,m.year"
	statements = statement1+statement2+statement3
	
	
	data ={	
		"statements" : [ {
    		"statement" : statements,
    		"parameters" : {
      			
        			"url" : url,
        			"year" : year
      			
    		}
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(res.status_code)
	r = res.json()
	result =  r['results'][0]['data']
	url = result[0]['row'][0]
	year = result[0]['row'][1]
	print(url,year)

def bandProperties():
	r = matchBand()
	n = r[0]
	result = r[1] 
	nrows = 0
	nrows = n
	print(nrows)
	for x in range(nrows-1):
		
		url = result[x]['row'][0]
		year = result[x]['row'][1]
		print(url)
		print(year)
		
		setBand(url,str(year))
	

def musicianProperties():
	r = matchMus()
	n = r[0]
	result = r[1] 
	nrows = 0
	nrows = n
	print(nrows)
	for x in range(nrows):
		url = result[x]['row'][0]
		year = result[x]['row'][1]
		print(url)
		print(year)
		setMus(url,str(year))

def getsubGenre()
	print("sending query get subGenre...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 ="MATCH (m:Musician)"
	statement2 = "Where not exists(m.genre)"
	statement3 = "Return m.fullName, m.name Limit " + limit
	statements = statement1+statement2+statement3

	data ={	
		"statements" : [ {
    		"statement" : statements
			
     			
     		   		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	print(res.status_code)
	
	nrows = countRows(res)

	r = res.json()
	rows = r['results'][0]['data'][0:nrows]
	print(r)
	subGenres = []
	for i in range(nrows):
		name =  rows[i]['row'][0]
		#print(name)
		subGenre = rows[i]['row'][1]
		subGenres.append((name,subGenre))
	#print(names)
	return(subGenres)

#def setGenre()	


if __name__ == '__main__':


	print("\n Downloading Test")

	
	x = 0 
	print("sending query...")
	for i in range(2000):
		
		time.sleep(5)
		musicianProperties()
		#bandProperties()
		
		

		x = x + 1
		print(x)
	
	


	
	
	





	#--------------------------------------------------------------------------------------
	#time and paths time and path implementation here 
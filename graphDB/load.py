from neo4j.v1 import GraphDatabase
#from band import Band
from relationships import Relationship
from Nodes.album import Album
from Nodes.musician import Musician
from Nodes.band import Band 
#from dynamic import Dynamic 
import requests
from neo4j.bolt import ProtocolError, ServiceUnavailable
from lxml import etree
from lxml import html

class Load():
	"""Move all loading functions from driver to this class"""
	
	driver = None

	def __init__(self, d):
		self.driver = d
		

	def mAlbum(self, url, musicianKey):
		album = Album(self.driver)
		rMA = Relationship(self.driver)
		rAM = Relationship(self.driver)
		album.setUrl(url)
		album.scrape()
		with self.driver.driver.session() as session:
			session.write_transaction(album.create)	
			#create album node connected to the musician
			session.write_transaction(album.index)
			session.write_transaction(rMA.recorded,musicianKey,album.getName())	#initialise a Musician->Album relationship
			session.write_transaction(rAM.recordedBy,album.getName(),musicianKey)	#initialise an Album -> Musician relationship
		session.close()


	def mBand(self, url, musicianKey):
		band = Band(self.driver)
		rMB = Relationship(self.driver)
		rBM = Relationship(self.driver)
		band.setUrl(url)
		band.scrape()
		with self.driver.driver.session() as session:
			session.write_transaction(band.create)		#create band node connected to the album
			session.write_transaction(band.index)
			session.write_transaction(rMB.performedIn, musicianKey, band.getName()) 	#initialise a Musician -> Band relationship
			session.write_transaction(rBM.hadMember, band.getName(), musicianKey)		#initialise a Band -> Musician relationship
		session.close()


	def bMusician(self, url,bandKey):
		musician = Musician(self.driver)
		rBM = Relationship(self.driver)
		rMB = Relationship(self.driver)
		musician.setUrl(url)
		musician.scrape()
		with self.driver.driver.session() as session:
			session.write_transaction(musician.create)
			session.write_transaction(musician.index)
			session.write_transaction(rBM.hadMember, bandKey,musician.getName())
			session.write_transaction(rMB.performedIn,musician.getName(),bandKey) 
		session.close()
	


	#Not connecting albums to bands as network is collaborative and bands are and albums are entities but not actors in the network as such
	def aMusician(self,url,albumKey):
		musician = Musician(self.driver)
		rAM = Relationship(self.driver)
		rMA = Relationship(self.driver)
		musician.setUrl(url)
		musician.scrape()
		with self.driver.driver.session() as session:
			session.write_transaction(musician.create)
			session.write_transaction(musician.index)
			session.write_transaction(rAM.recordedBy,albumKey,musician.getName())#musician to album relationship 
			session.write_transaction(rMA.recorded, musician.getName(),albumKey) #album to musician relationship
		session.close()
		

		


	def loadMusician(self, url):
		'''
		Loads musician node into the database first by setting the url of the page to be scraped
		Then scraping the relevant data for the node, it's relationships and properties.
		Creating that node, and index on the node, calling the relavant node and relationship creation functions.
		'''
		path = url.replace("http://www.bandtoband.com","")
		musician = Musician(self.driver)
		#Call setbands and set albums to empty first 
		musician.setBands(dict())
		musician.setAlbums(dict())
		musician.setUrl(path) #"/artist/dolores-oriordan"
		musician.scrape()

		with self.driver.driver.session() as session:
			session.write_transaction(musician.create)
			session.write_transaction(musician.index)
		session.close()

		albums = {}
		albums.clear()
		bands = {}
		bands.clear()
		print("musician node created") #remove later
		musicianKey = musician.getName() # the key or unique identifier for the node
		print(musicianKey + " relationships")
		albums = musician.getAlbums()

		for urla in albums.values():
			if self.checkValid("http://www.bandtoband.com"+urla):
				self.mAlbum(urla,musicianKey)
				#print(urla,musicianKey)
			else:
				continue

		bands = musician.getBands()
		for urlb in bands.values():
			if self.checkValid("http://www.bandtoband.com"+urlb):
				self.mBand(urlb,musicianKey)
				#print(urlb,musicianKey)
			else:
				continue

		
		
		print("Musician relationships created")




	def loadBand(self, url):
		'''
		Loads band node into the db first by seeting url, then scraping that pages url.
		Then scraping information about the relevant related nodes and connecting them via relationships
		Remember to uncomment collaboratedWith
		'''
		path = url.replace("http://www.bandtoband.com","")
		band = Band(self.driver)
		band.setMusicians(dict())
		band.setAlbums(dict())
		band.setUrl(path)
		band.scrape()

		with self.driver.driver.session() as session:
			session.write_transaction(band.create)
			session.write_transaction(band.index)
		session.close()

		bandmusicians = {} #bandmember
		bandmusicians.clear()
		#bandalbum = {}
		#bandalbum.clear()
		print("band node created")
		#create related nodesand subse
		bandKey = band.getName() # key or unique indentifying string for the band node
		print(bandKey + " relationships")
		#why does this list of musicians also contain members of the bands from the previous album
		bandmusicians = band.getMusicians()
		#bandalbum = band.getAlbums()
		for urlm in bandmusicians.values():
			if self.checkValid("http://www.bandtoband.com"+urlm):
				self.bMusician(urlm,bandKey)
				#print(urlm,bandKey)
			else:
				continue
		print("Band relationships  created")



	def loadAlbum(self, url):
		path = url.replace("http://www.bandtoband.com","") # remember to move in load class
		album = Album(self.driver)
		album.setMusicians(dict())
		album.setBand(dict())
		album.setUrl(path)
		album.scrape()

		with self.driver.driver.session() as session:
			session.write_transaction(album.create)
			session.write_transaction(album.index)
		session.close()
		
		albummusicians = {}
		albummusicians.clear()
		print("album node created")
		albumKey = album.getName()
		print(albumKey + " relationships")
		albummusicians = album.getMusicians()
		for urlm in albummusicians.values():
			if self.checkValid("http://www.bandtoband.com"+urlm):
				self.aMusician(urlm,albumKey)
				#print(urlm,albumKey)
			else:
				continue

		print("album relationships created")
		#same thing for album-band
		
		#two albums are not connected
	'''
	def musicianYear(self,urls):
		time = Musician(self.driver)
		
		for url in urls:
			result = 0 
			with self.driver.driver.session() as session:
			#Cypher write transaction with return (dynamic.musicians)
				url = url.replace("http://www.bandtoband.com","")
				result = session.read_transaction(time.getYear,url)
				
				if result == 'None':
					continue
				#print(result.single()[0])
				session.write_transaction(time.setYear,url,result)
			session.close()
	
	
	def bandYear(self,urls):
		time = Band(self.driver)
	
		for url in urls:
			result = 0
			with self.driver.driver.session() as session:
			#Cypher write transaction with return (dynamic.musicians)
				url = url.replace("http://www.bandtoband.com","")
				result = session.read_transaction(time.getYear,url)


				if result == 'None':
					continue
				#print(urls.index(url),"/", len(urls))
				session.write_transaction(time.setYear,url,result)
			session.close()
	'''
		

	def collaboratedWith(self):
		b1 = '' #band1
		b2 = '' #band2
		y = 0 #year album was recorded
		a = '' #the album title
		count = 0
		collab= Relationship(self.driver)

		with self.driver.driver.session() as session:
			try :
				result = session.read_transaction(collab.query)
			#Do thins to that result producing the 
			#variables to be passed in.
			except (ServiceUnavailable):
				print("retrying...")
				result_ = session.read_transaction(collab.query)
				result.append(result_)
			for record in result:
				#set method variable to result values
				b1 = record["b.name"]
				b2 = record["c.name"]
				y = record["a.year"]
				a = record["a.title"] 
				session.write_transaction(collab.create,b1,b2,y,a)
				
				count = count + 1
				total = len(result)

				print( count, "/",total," paths created")

		session.close()
		print("all paths created")

	def recordedWith(self):
		#rec= Relationship(self.driver) #Relationship between two musicians
		pass

	def checkValid(self,url):
		try:
			response = requests.get(url)
			tree = etree.HTML(response.text)
			title = tree.cssselect('title')[0]

		except (requests.exceptions.Timeout,  requests.exceptions.RequestException, IndexError, requests.exceptions.ConnectionError ):
			response = requests.get(url)
			tree = etree.HTML(response.text)
			title = tree.cssselect('title')[0]

		if title.text == 'Search Results - BandToBand.com':
			return False

		elif title.text == 'BandToBand.com':
			return False

		elif title.text == '':
			return False

		else:
			return True
		return True 
import gzip 
import requests
from lxml import etree
import cssselect


class Scrape():
	"""docstring for Scrape"""
		

	#self.UrlList = " "


	def unZip(self, fileName):
		#call self.fileName no need to pass any arguemnts into this function
		f = gzip.GzipFile(fileobj=open(fileName, 'rb'))
		#Unzips file and loads data into new variable xml conent
		xmlContent = f.read()
		return xmlContent

	def downloadZip(self,url,zipname):
		'''
		Open Url adress of the xml sitemap,read the content of the xml page
		assign a file for the sitemap to be read to,open the newly assigned file.
		write the content read from the xml sitemap into the new fil
		create variable xml conect
		unzip the lxml content by calling the unzip function 
		call unzip function on content and the place value into xml content variable.

		'''
		path = 'webdata'
		
		response = requests.get(url)
		fileName = path + '/' + zipname
		file = open(fileName, 'wb')
		#print(response.content)
		file.write(response.content)
		file.close()
		return fileName
		

	def downloadBand(self):
		sitemapUrl = 'http://www.bandtoband.com/sitemaps/sitemap.bands.xml.gz'
		zipname = 'bands.xml.gz'
		fileName = self.downloadZip(sitemapUrl,zipname)
		xmlContent = self.unZip(fileName)
		tree = etree.fromstring(xmlContent)
		ns = {"m": tree.tag.split("}")[0][1:]}
		urlList = tree.xpath('//m:url/m:loc/text()',namespaces=ns)
		bandUrlList = []
		for url in urlList :
			if '/band/'  in url:
				bandUrlList.append(url)
			else: 
				continue
		print("band urls downloaded from sitemap")
		return bandUrlList
		
		
		
	def downloadMusician(self):
		sitemapUrl = 'http://www.bandtoband.com/sitemaps/sitemap.artists.xml.gz'
		zipname = 'artists.xml.gz'
		fileName = self.downloadZip(sitemapUrl,zipname)
		xmlContent = self.unZip(fileName)
		tree = etree.fromstring(xmlContent)
		ns = {"m": tree.tag.split("}")[0][1:]}
		urlList = tree.xpath('//m:url/m:loc/text()',namespaces=ns)
		musicianUrlList = []
		for url in urlList :
			if '/artist/'  in url:

				musicianUrlList.append(url)
			else: 
				continue
		print("musician urls downloaded from sitemap")
		return musicianUrlList


	def downloadAlbum(self):
		sitemapUrl = 'http://www.bandtoband.com/sitemaps/sitemap.albums.xml.gz'
		zipname = 'albums.xml.gz'
		fileName = self.downloadZip(sitemapUrl,zipname)
		xmlContent = self.unZip(fileName)
		tree = etree.fromstring(xmlContent)
		ns = {"m": tree.tag.split("}")[0][1:]}
		urlList = tree.xpath('//m:url/m:loc/text()',namespaces=ns)
		albumUrlList = []
		for url in urlList :
			if '/band/'  in url : #If valid is true the page is not blank 
				albumUrlList.append(url)
			else: 
				continue
		print("album urls downloaded from sitemap")
		
		return albumUrlList



#driver and name passed into the constructor

#attributes are properties 
#methods are crud operations 
import time 
import requests
from lxml import etree
from lxml import html

from fake_useragent import UserAgent


class Musician(): 

	name = " " #key --done
	url = " " # --done
	driver = None #--done
	fullName = " "  #--done
	
	#Relationships
	albums = {} #added using funtion #albums
	albums.clear()
	bands = {} #potentially added later this would helped the performed in relationship #set 
	bands.clear()
#-----------------------------------------------------------------------------------------------------------------------------
	#Properties
	otherNames = set() #---> list of other stage names could also just be empty much sure functions have case for empty set
	otherNames.clear()
#--------------------------------------------------------------------------------------------------------------------------
	 

	def __init__(self, driver):
		self.driver = driver

	#Methods include getters setters and crud operations
	#get and set <-------------- <---------------- <--------------------

	def setName(self,name):
		self.name = name

	def getName(self):
		return self.name


	def getFullName(self):
		return self.fullName

	def setAlbums(self,albums):
		self.albums = albums

	def getAlbums(self):
		return self.albums

	def getAlbumyears(self):
		return self.albumyears

	def setBands(self,bands):
		self.bands = bands
		
	def getBands(self):
		return self.bands

	def getOtherNames(self):
		return self.otherNames


	def setUrl(self, url):
		self.url = url

	def getUrl(self):
		return self.url




	def create(self,tx):
		name = self.name
		fullName = self.fullName
		tx.run( "MERGE (m:Musician {name: {name}, fullName: {fullName}})", name=name, fullName=fullName )
			


	def index(self,tx):
		tx.run("CREATE INDEX ON :Musician(name,fullName)")
			#this ensures only nodes in which name and fullName are present will be created


	def getYear(self,tx,name):
		print(name)
		
		result = tx.run("OPTIONAL MATCH (m:Musician {name: {name}} )--(a:Album) RETURN min(a.year)", name=name)
		return result.single()[0]

	def setYear(self,tx,name,year):
		result = tx.run("MATCH (m:Musician {name: {name}}) SET m.year = {year} RETURN m.year", name=name, year=year)
		






	def scrape(self):
		ua = UserAgent()
		headers = {'User-Agent' : ua.random}

		url = "http://www.bandtoband.com" + self.url
		try:
			response = requests.get(url,headers=headers) 
			tree = etree.HTML(response.text)

			self.name = self.url

			fullName = tree.cssselect('h1')[0]  # equivalent to previous XPath
			self.fullName = fullName.text

			albums = tree.cssselect('div[class=artistAlbumTitle] a')
			self.albums.clear()
			for i in albums :
				self.albums[i.text ] = i.attrib['href'] 

			bands = tree.cssselect('div[class=artistAlbumInfo] > :nth-child(1) > a')
			self.bands.clear()
			for b in bands:
				self.bands[b.text] = b.attrib['href']

			otherNames = tree.cssselect('span')
			for oN in otherNames:
				if oN.text != None:
					self.otherNames.add(oN.text)

		except (requests.exceptions.Timeout,  requests.exceptions.RequestException, IndexError,requests.exceptions.ConnectionError ):
			print('retrying...') # delay for five seconds then send the request again 
			time.sleep(5)
			response = requests.get(url,headers=headers) 
			tree = etree.HTML(response.text)

			self.name = self.url

			fullName = tree.cssselect('h1')[0]  # equivalent to previous XPath
			self.fullName = fullName.text

			albums = tree.cssselect('div[class=artistAlbumTitle] a')
			self.albums.clear()
			for i in albums :
				self.albums[i.text ] = i.attrib['href'] 

			bands = tree.cssselect('div[class=artistAlbumInfo] > :nth-child(1) > a')
			self.bands.clear()
			for b in bands:
				self.bands[b.text] = b.attrib['href']

			otherNames = tree.cssselect('span')
			for oN in otherNames:
				if oN.text != None:
					self.otherNames.add(oN.text)

		
	




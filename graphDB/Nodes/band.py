#driver and name passed into the constructor

#attributes are properties 
#methods are crud operations 
import time 
import requests
from lxml import etree
from lxml import html

from fake_useragent import UserAgent

class Band(): 

	name = " " #key
	
	driver = None #passed in on initialisation
	url = " "

	#Relationships
	musicians = {} #added using funtion #Musicians
	musicians.clear()
#-------------------------------------------------------------------------------------------
	#Properties
	bandName = " "
	albums = {} # not dictionary as only artists are connected to the albums they produce indicating which line ups of said band produced that album.
	albums.clear()
#-------------------------------------------------------------------------------------------



	#Methods include initialiser getters setters and crud operations
	def __init__(self,driver):
		self.driver = driver
	
	def getBandName(self):
		return self.bandName	

	def setBandName(self,bandName):
		self.bandName = bandName


	def getName(self):
		return self.name

	
	def getMusicians(self):
		return self.musicians #returns list of band members

	def setMusicians(self,musicians):
		self.musicians = musicians

	def getUrl(self):
		return self.url

	def setUrl(self,url):
		self.url = url	

	def getAlbums(self):
		return self.albums

	def setAlbums(self,albums):
		self.albums = albums




	

	#dont pass band members in using a constructor handle it using a band function
	def create(self,tx): #other properties
		name = self.name
		bandName = self.bandName
		albumlist = self.albums.keys()
		albums = ','.join(albumlist) # converts albums dictionary values to string
		tx.run( "MERGE (b:Band {name: {name}, bandName:{bandName}, albums:{albums}})", name=name, bandName=bandName, albums=albums )
		self.albums.clear()

	

	def index(self,tx):
		tx.run("CREATE INDEX ON :Band(name,bandName)")
		
	
	def getYear(self,tx,name):
		print(name)
		result = tx.run("OPTIONAL MATCH (b:Band {name:{name}})--(m:Musician)--(a:Album) Where b.albums contains a.title  Return min(a.year)", name=name)
		return result.single()[0]

	def setYear(self,tx,name,year):
		result = tx.run("MATCH (b:Band {name: {name}}) SET b.year = {year} RETURN b.year", name=name, year=year)
	


	def scrape(self):
		ua = UserAgent()
		headers = {'User-Agent': ua.google }
		url = "http://www.bandtoband.com" + self.url
		try:
			response = requests.get(url, headers=headers) 
			tree = etree.HTML(response.text)

			self.name = self.url

			bandName = tree.cssselect('h1')[0]  # equivalent to previous XPath
			self.bandName = bandName.text

			members = tree.cssselect('td[class=bandMember] li a') #scrape bandmembers
			self.musicians.clear()
			for m in members:
				self.musicians[m.text ] = m.attrib['href'] #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

			albums = tree.cssselect('[class=bandAlbumTitle]  a')
			self.albums.clear()
			for a in albums:
				self.albums[a.text] = a.attrib['href']

		except (requests.exceptions.Timeout,  requests.exceptions.RequestException, IndexError,requests.exceptions.ConnectionError ):
			print('retrying...')
			time.sleep(5) # delay for five seconds then send the request again
			response = requests.get(url, headers=headers) 
			tree = etree.HTML(response.text)

			response = requests.get(url, headers=headers) 
			tree = etree.HTML(response.text)

			self.name = self.url

			bandName = tree.cssselect('h1')[0]  # equivalent to previous XPath
			self.bandName = bandName.text

			members = tree.cssselect('td[class=bandMember] li a') #scrape bandmembers
			self.musicians.clear()
			for m in members:
				self.musicians[m.text ] = m.attrib['href'] #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

			albums = tree.cssselect('[class=bandAlbumTitle]  a')
			self.albums.clear()
			for a in albums:
				self.albums[a.text] = a.attrib['href']

		


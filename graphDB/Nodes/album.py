#driver and name passed into the constructor

#attributes are properties 
#methods are crud operations 
import time 
import requests
from lxml import etree
from lxml import html
#from bs4 import BeautifulSoup
from fake_useragent import UserAgent



class Album(): 

	name = " " #key -done
	#link could also be potential property
	driver = None
	url = " " #-done url on bandtoband
	 
	

	#Relationships
	musicians = {} #connect to band members #name goes in key and url goes into value {full name : link} -done
	musicians.clear()

	#Properties
	title = " "
	year = 0 
	band = " "


	

	def __init__(self, driver):
		self.driver = driver
	#----------------------------------------------------------------------------------------
	#Getters and setters

	def setName(self,name):
		self.name = name

	def getName(self):
		return self.name

	def setTitle(self, title):
		self.title = title


	def getTitle(self):
		return self.title

	def setYear(self,year):
		self.year = year

	def getYear(self):
		return self.year


	def setMusicians (self, musicians):
		self.musicians = musicians

	def getMusicians(self):
		return self.musicians

	def getBand(self):
		return self.band

	def setBand(self, band):
		self.band = band 


	def setUrl(self,url):
		self.url = url	
#---------------------------------------------------------------------------------------------------
	#Crud operations


	def create(self, tx):
		name = self.name
		title = self.title
		year = self.year
		band = self.band
		tx.run( "MERGE (a:Album {name: {name}, title: {title}, year: {year}, band: {band}})", name=name, title=title, year=year, band=band)
				


	def index(self, tx):
		tx.run("CREATE INDEX ON :Album(name,title,year)")
			#use CALL db.indexes in cypher terminal to see changes


	def scrape(self): #band and album might be params 
		#the correct link should be parsed in from a function in graph db called from another node possibly
		#combination of link and band in album scrape 
		ua = UserAgent()
		headers = {'User-Agent' : ua.random} # 'connection': 'keep-alive', http://urllib3.readthedocs.io/en/1.2.1/pools.html

		url = "http://www.bandtoband.com" + self.url
		
		try:
			response = requests.get(url, headers = headers)
			tree = etree.HTML(response.text) 

			self.name = self.url

			title = tree.cssselect('div[class=albumTitle] h2')[0]
			self.title = title.text
		

			year = tree.cssselect('div[class=albumYear]')[0]
			self.year = int(year.text)


			band = tree.cssselect('div[class=albumBand] h1 a')[0]
			self.band = None
			self.band = band.text
		
			members = tree.cssselect('div[class=albumMembers] a')
			self.musicians.clear()
			for i in members:
				self.musicians[i.text ] = i.attrib['href']


		except (requests.exceptions.Timeout,  requests.exceptions.RequestException, IndexError,requests.exceptions.ConnectionError ):
			print('retrying...')
			time.sleep(5) # delay for five seconds then send the request again
			response = requests.get(url, headers = headers) 
			tree = etree.HTML(response.text)#

			self.name = self.url

			title = tree.cssselect('div[class=albumTitle] h2')[0]
			self.title = title.text
		

			year = tree.cssselect('div[class=albumYear]')[0]
			self.year = int(year.text)

			band = tree.cssselect('div[class=albumBand] h1 a')[0]
			self.band = band.text
		
			members = tree.cssselect('div[class=albumMembers] a')
			for i in members:
				self.musicians[i.text ] = i.attrib['href']


	

		

		
	


	


#from dynamic import Dynamic
import requests
import json
import time

#https://neo4j.com/docs/developer-manual/current/http-api/
#


def createBB(bandb,bandc,year,weight):
	print(bandb,bandc,weight,year)

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 ="MATCH (b:Band {name: {bandb}}),(c:Band {name: {bandc}})"
	statement2 = "MERGE p = (b)-[r:HaveCollaborated {name: b.bandName + '<->' + c.bandName, year: {year} , weight: {weight}} ]->(c)"
	statement3 = "Return p"
	statements = statement1+statement2+statement3

	data ={	
		"statements" : [ {
    		"statement" : statements,
			"parameters" : {
      			
        			"bandb" : bandb,
        			"bandc" : bandc,
        			"year" : year,
        			"weight" : weight
     			
     		 }    		
  		} ]
	}

	#print(data)

	data = json.dumps(data)
	#separate out match and merge queries pass values as paramas from one to another
	r = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(r.status_code) #prints the reponse
	#print(r.json())
	r = r.json()
	print('relationship created')


def createMM(musA,musB,weight,year):
	print(musA,musB,year,weight)
	
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 ="MATCH (m:Musician {name: {musA}}),(n:Musician {name: {musB}})"
	statement2 = " MERGE p =(m)-[r:RecordedWith {name: m.fullName + '<->' + n.fullName,  year: {year} , weight: {weight} }]->(n)"
	statement3 = " Return p"
	statements = statement1+statement2+statement3

	data ={	
		"statements" : [ {
    		"statement" : statements,
			"parameters" : {
      			
        			"musA" : musA,
        			"musB" : musB,
        			"year" : year,
        			"weight" : weight
     			
     		 }    		
  		} ]
	}
	data = json.dumps(data)
	#separate out match and merge queries pass values as paramas from one to another
	r = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(r.status_code) #prints the reponse
	#print(r.json())
	r = r.json()
	print('relationship created')
	


def bandPath():
	print("sending query...")

	limit = "300"
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (m:Musician)-[:PerformedIn]->(b:Band),(n:Musician)-[:PerformedIn]->(c:Band),(m:Musician)-[:Recorded]->(a:Album), (n:Musician)-[:Recorded]->(z:Album)"
	statement2 = " WHERE a.name = z.name AND NOT b.name = c.name"
	statement3 = " AND NOT (b)-[:HaveCollaborated]-(c)"
	statement4 = " Return b.name,c.name,count( a) AS nAlbums, min(a.year) AS  year LIMIT " +limit #Distinct A
	statements = statement1+statement2+statement3+statement4
 
	
	data ={	
		"statements" : [ {
    		"statement" : statements
  		} ]
	}

	data = json.dumps(data)

	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print(res.status_code) #prints the reponse
	

	r = res.json()
	print(r) # this value will be used to count all of a specific singularly occuring word count occurences in sting meta/row
	
	#print(r['results'][0]['columns'][0:3]) #first -4thcolum
	
	

	n= 0
	l = range(int(limit))
	for x in l:
		bandb = r['results'][0]['data'][x]['row'][0] #data in first coloum of X row
		bandc = r['results'][0]['data'][x]['row'][1] #data in second cloum of X row
		nAlbums = r['results'][0]['data'][x]['row'][2] #data in ... coloum of X row #weight of each link
		year = r['results'][0]['data'][x]['row'][3] #data in ... colum of X row 
		#print('\n')
		
		createBB(bandb,bandc,year,nAlbums)
		n = n + 1
		print(n)
	print("created:", n ," bands")


def musicianPath():
	
	print("sending query...")

	limit = "300"

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (m:Musician)-[p:Recorded]->(a:Album), (n:Musician)-[q:Recorded]->(b:Album)"
	statement2 = " WHERE a.name = b.name AND NOT m.name = n.name"
	statement3 = " AND NOT (m)-[:RecordedWith]-(n)"
	statement4 = " Return m.name,n.name,count( a) AS nAlbums, min(a.year) AS  year LIMIT " +limit #Distinct A
	statements = statement1+statement2+statement3+statement4
 
	
	data ={	
		"statements" : [ {
    		"statement" : statements
  		} ]
	}

	data = json.dumps(data)

	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print(res.status_code) #prints the status reponse
	#print(r.json())
	

	r = res.json()
	#print('The type of the results is', type(r)) # this value will be used to count all of a specific singularly occuring word count occurences in sting meta/row
	nrows = countRows(res)
	print(r['results'][0]['columns'][0:3]) #first -4thcolum
	n= 0
	#l = range(int(limit))

	for x in range(nrows):
		musA = r['results'][0]['data'][x]['row'][0] #data in first coloum of X row
		musB = r['results'][0]['data'][x]['row'][1] #data in second cloum of X row
		nAlbums = r['results'][0]['data'][x]['row'][2] #data in ... coloum of X row #weight of each link
		year = r['results'][0]['data'][x]['row'][3] #data in ... colum of X row 

		createMM(musA,musB,nAlbums,year)
		n = n + 1
		print(n)
	
	print("created:", n ," albums")

def countRows(res):
	text = res.text
	n = text.count("row")
	return n

if __name__ == '__main__':
	print("\n Creating Paths")

	x = 0 

	for i in range(1000):
		#try:
			bandPath()
			time.sleep(15)

			musicianPath() #put in for loop and run ten times
			time.sleep(15)
			x = x + 1
			print("Number of iterations:",x)
		
		#except(IndexError):
			#print("Caught Up")




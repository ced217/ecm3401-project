from neo4j.v1 import GraphDatabase


#use bin\neo4j start to run server

'''
This is where the run all will most likely be stored and 
where the driver object instance will be instanciated
and the db loading functions for the scraped data
'''

#db loading functions if necessary will pass data from the webscraper to the neo4j create functions

#initialistion 

class Driver:

	 #bolt://127.0.0.1:7687
	driver = None




	def __init__(self):
		uri = "bolt://localhost:7687"
		self.driver = GraphDatabase.driver(uri, auth=("neo4j", "chaoticImp291295")) #connection_timeout= 1000?

		#max_connection_lifetime=mLifeTime,max_connection_pool_size=mSize,connection_acquisition_timeout=mTimeOut,



	def close(self):
		self.driver.close()



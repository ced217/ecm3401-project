import spotipy
import spotipy.util as util
from spotipy.oauth2 import SpotifyClientCredentials

import requests
import json
import time


def countRows(res):
	text = res.text
	n = text.count("row")
	return n

def getBands(limit): #to http endpoint returns list of names 
	print("sending query...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 ="MATCH (b:Band )"
	statement2 = "Where not exists(b.genre)"
	statement3 = "Return b.bandName,b.name Limit "+ limit 
	statements = statement1+ statement2+statement3

	data ={	
		"statements" : [ {
    		"statement" : statements
			
     			
     		   		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	print(res.status_code)
	
	nrows = countRows(res)

	r = res.json()
	rows = r['results'][0]['data'][0:nrows]
	print(r)
	names = []
	for i in range(nrows):
		name =  rows[i]['row'][0]
		url = rows[i]['row'][1]
		#print(url)
		names.append((name,url))
	#print(names)
	return(names)


#dbnames = []

def getMusicians(limit):
	print("sending query...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 ="MATCH (m:Musician)"
	statement2 = "Where not exists(m.genre)"
	statement3 = "Return m.fullName, m.name Limit " + limit
	statements = statement1+statement2+statement3

	data ={	
		"statements" : [ {
    		"statement" : statements
			
     			
     		   		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	print(res.status_code)
	
	nrows = countRows(res)

	r = res.json()
	rows = r['results'][0]['data'][0:nrows]
	print(r)
	names = []
	for i in range(nrows):
		name =  rows[i]['row'][0]
		#print(name)
		url = rows[i]['row'][1]
		names.append((name,url))
	#print(names)
	return(names)

def setBands(url,genre):
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (b:Band {name: {url}})"
	statement2 = " SET b.genre = {genre}"
	statement3 = " RETURN b.name,b.genre"
	statements = statement1+statement2+statement3
	
	
	data ={	
		"statements" : [ {
    		"statement" : statements,
    		"parameters" : {
      			
        			"url" : url,
        			"genre" : genre
      			
    		}
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(res.status_code)
	r = res.json()
	#print(r)
	result =  r['results'][0]['data']

	url = result[0]['row'][0]
	genre = result[0]['row'][1]
	#print(url,genre)

def setMusicians(url,genre):
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (m:Musician {name: {url}})"
	statement2 = " SET m.genre = {genre}"
	statement3 = " RETURN m.name,m.genre"
	statements = statement1+statement2+statement3
	
	
	data ={	
		"statements" : [ {
    		"statement" : statements,
    		"parameters" : {
      			
        			"url" : url,
        			"genre" : genre
      			
    		}
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(res.status_code)
	r = res.json()
	result =  r['results'][0]['data']
	url = result[0]['row'][0]
	genre = result[0]['row'][1]
	#print(url,genre)


def getGenres(name):
	aid = ''
	cid = 'c95413e7734e423da94369d05d1f806f'
	csecret = '2762a2320e944f7e99da9f8927971946'
	client_credentials_manager = SpotifyClientCredentials(cid,csecret)
	sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
	#sp.trace = True # turn on tracing
	#sp.trace_out = True # turn on trace out
	try:
		res = sp.search(q='artist:' + name, type='artist')
		genre = res['artists']['items'][0]['genres']
	except(spotipy.client.SpotifyException, requests.exceptions.HTTPError):
		genre = []
	return genre

def getsubGenres(limit,label):
	print("sending query get subGenre...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 ="MATCH (n:"+label+")"
	statement2 = "Where exists(n.genre) AND NOT exists(n.maingenre) AND NOT n.genre = []  "
	statement3 = "Return n.genre, n.name Limit " + limit
	statements = statement1+statement2+statement3

	data ={	
		"statements" : [ {
    		"statement" : statements
			
     			
     		   		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	print(res.status_code)
	
	nrows = countRows(res)

	r = res.json()
	rows = r['results'][0]['data'][0:nrows]
	#print(r)
	#print(rows)
	#print(rows[2]['row'][0])
	subGenres = []
	for i in range(int(limit)):
		subGenre =  rows[i]['row'][0]
		#print(name)
		name = rows[i]['row'][1]
		subGenres.append((name,subGenre))
	#print(subGenres)
	return(subGenres)


def setMainGenres(url,maingenre,label):
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (n:"+label+" {name: {url}})"
	statement2 = " SET n.maingenre = {maingenre}"
	statement3 = " RETURN n.name,n.maingenre"
	statements = statement1+statement2+statement3
	
	
	data ={	
		"statements" : [ {
    		"statement" : statements,
    		"parameters" : {
      			
        			"url" : url,
        			"maingenre" : maingenre
      			
    		}
  		} ]
	}

	data = json.dumps(data)


	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	#print(res.status_code)
	r = res.json()
	print(r)
	
	result =  r['results'][0]['data']

	url = result[0]['row'][0]
	genre = result[0]['row'][1]
	return(url,genre)

if __name__ == '__main__':
	
	limit = 1
	bt= 0
	mt = 0
	for i in range(1000):
		time.sleep(5)
		bnames = getBands(str(int(limit)))
		mnames = getMusicians(str(limit))
		bc = 0
		mc = 0
	
		for b in bnames:
			#print(n)
			try:
				#first element of b
				genre = getGenres(b[0])
				print(b[1],genre) #these will become community labels
				setBands(b[1],genre)

			except(IndexError):
				genre = []
				print(b[1],genre)
				setBands(b[1],genre)
				continue
			bc = bc +1

		for m in mnames:
			#print(n)
			try:
				#first element of m
				genre = getGenres(m[0])


				print(m[0],m[1],genre) #these will become community labels
			
				setMusicians(m[1],genre)
			except(IndexError):
				genre = []
				print(m[0],m[1],genre)
				#print(m,mkey,genre)
				setMusicians(m[1],genre)
				continue
			mc = mc + 1

		bt = bt + bc
		mt = mt + mc 
		print("number of bands:", bt)
		print("number of musicians:", mt)




	#MATCH (b:Band) Where not exists b.genre
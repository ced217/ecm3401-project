
from relationships import Relationship
from Nodes.album import Album
from Nodes.musician import Musician
from Nodes.band import Band
from scrape import Scrape
from load import Load 
from driver import Driver
import requests

#use bin\neo4j start to run server

'''
This is where the run all will most likely be stored and 
where the driver object instance will be instanciated
and the db loading functions for the scraped data
'''

#db loading functions if necessary will pass data from the webscraper to the neo4j create functions

#initialistion 



if __name__ == '__main__':
	



	print("\n Downloading Test")

	driver = Driver() # creating the driver object but other way round next time call create nodes in graph db
	scrape = Scrape()
	load = Load(driver)
	path = Relationship(driver)
	


	albumUrlList = scrape.downloadAlbum()

	i =  -964 #back index from findIndex must always be negative- to ensure only last x are tekn
	httpalbums =  albumUrlList[i:]
	#['http://www.bandtoband.com/band/slipknot-us-2/25','http://www.bandtoband.com/band/slipknot-us-2/all-hope-is-gone','http://www.bandtoband.com/search/?']
		#
	#for loop on test to pass urls into loading functions  

	print ("\n Uploading...")

	#makegraph(urlList)
	
	
	for urla in httpalbums:
		valid = load.checkValid(urla)
		if valid:
			try:
				count = albumUrlList.index(urla)
				total = len(albumUrlList)
				percent = count/total * 100
				print(urla," urls downloaded:", count,'/',total,' ',percent,'%')
				
				load.loadAlbum(urla)
			except requests.exceptions.TooManyRedirects:
				continue
		else:
			continue

	print ("DONE!!!!")
	

	#load.createPaths() # creates relationships between muscians and band nodes based on wether there is a path connecting them
	#path.collaboratedWith()
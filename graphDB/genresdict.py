#dictionary with list, each genre has a list of buzz words, subgenres, genres attributed to it, if it shows up in the list
from subgenres import getsubGenres,setMainGenres
import collections as col


#Checking if any of the keywords are in the genre list.

'''
compare 5 words from each list in dict/list of lists 
take the first subgenre list and check if any of the keywords match (regex) 
if there is a match append genre name to list count all in list highest value is genre
if two then chosen at random

'''

def genrefromSub(name,subgenres,genres):
	print('converting...')
	#subgenres current neo4j attribute known as genre, this is a list of subgenres
	#not a bug its a feature
	#works for 2 because it has both in the name
	mgs = [] #list of main genres decided from subgenres  
	print(subgenres)
	for x in subgenres:
		subg = x # a single subgenre in the list of subgenres 
		#print(subg)
		for k,v in genres.items():
			for keyword in v: #for each key word in the list of genre key words
				if keyword not in subg: #check that the key word isn't in the list 
					continue # if it isnt move on 
				else: #it is add not the keyword but the main genre to the list
					mgs.append(k)

					
					


		print('trace',name,mgs) #trace shows name over time
	
	if not mgs: 
		maingenre = None
	else :
		maingenre = countGenres(mgs)	

	print(maingenre)
	return (name,maingenre)
	#takes subgenre checks what main genre it belongs to the returns the main genre

def countGenres(genres):
	print('tallying...')
	#takes list of all main decided genres, then runs a modal vote on the the highest one is the genre for that band 
	#if more than one max either function will sort it or 
	main = col.Counter(genres).most_common(1)
	
	main = main[0][0]# returns tuple with number # use counter for more genre information.
	#print(main)
	return main
#http://www.musicgenreslist.com/
#https://en.wikipedia.org/wiki/Category:Musical_subgenres_by_genre

def runAll(genres):
	
	limit = 20
	label = 'Musician'
	a = getsubGenres(str(limit),label) #params = limit,label
	for x in range(limit):
		namemainmgs = genrefromSub(a[x][0],a[x][1],genres)	 # params = name, subgenres,genres
		name = namemainmgs[0]
		maingenre = namemainmgs[1]
		#mgs =namemainmgs[2]
		
		if maingenre != None:
			
			smg = setMainGenres(name,maingenre,label)
			print(smg)

		else:
			continue
			
	limit = 10
	label = 'Band'
	a = getsubGenres(str(limit),label) #params = limit,label
	for x in range(limit):
		namemainmgs = genrefromSub(a[x][0],a[x][1],genres)	 # params = name, subgenres,genres
		name = namemainmgs[0]
		maingenre = namemainmgs[1]
		#mgs =namemainmgs[2]
		
		if maingenre != None:
			smg = setMainGenres(name,maingenre,label)
			print(smg)

		else:
			#continue
			smg = setMainGenres(name,'N/A',label)
			print(smg)


if __name__ == '__main__':
		
	#a = getsubGenres('30','Musician') #params = limit,label for x in limit x is the var at 0
	#returns all the names and all genres work inside a for loop of this  move up to general functions?
	#print(a)
	#name = '/artist/cyndi-lauper'#'/artist/jack-white-us-2' #a[0][0]	
	#subgenres = ['dance pop', 'dance rock', 'europop', 'new romantic', 'new wave pop', 'permanent wave', 'pop rock', 'rock', 'soft rock']#['alternative rock', 'blues-rock', 'garage rock', 'modern blues', 'modern rock', 'punk blues']#a[0][1]
	#a[0][1][0] this accesses the first element in the list of subgenres	
	#b = setMainGenres(a[0][0],a[0][1],'Musician') #params = name,
	#print(b)




	blues = ["saxophone","blues", "acoustic-blues","chicago-blues","classic-blues","contemporary-blues","country-blues","electric-blues","ragtime"]
	classical = ["classical","cello","vaudeville","choral","ensemble","orchesta","chamber","classical performance"]
	country = ["country","bluegrass","southern"]
	dance = ["dance","club"]
	electronic = ["trance","electronica","electronic","breakbeat","chicago-house","house", "deep-house","detroit-techno","techno","drum-and-bass", "dubstep", "edm","garage","electro", "glitch","EDM","step"]
	experimental = ["experimental","art-punk","experimental-rock","avant-garde", "free improvisation", "microtonal","krautrock","psychedelic"]
	folk = ["folk","acoustic"]
	hiphop = ["hip hop","rap","trap","grime"]
	indie = ["indie","indie-rock", "lo-fi","shoegaze","shoegazing","wave"]
	jazz = ["jazz","bossanova","bigband","swing"]
	metal = ["metal", "death-metal","core","heavy-metal","black","doom","djent", "jazz metal", "mathcore","industrial","screamo","thrash"]
	pop = ["pop","acoustic"]
	punk = ["punk","hardcore-punk","crust-punk", "folk-punk","new-wave","steampunk","ska","riot grrrl","straight edge"]
	reggae = ["reggae","dancehall","dub"]
	religious = ["religious","christian","worship"]
	rnb = ["soul","funk","disco","motown"]
	rock = [ "emo","alt-rock","rock","hard-rock","alternative-rock", "post-hardcore","experimental rock", "goth", "gothic rock", "grunge","pop punk","poppunk","progressive rock"]
	world = ["balkan brass", "balkan","gypsy-punk", "klezmer","world","latin","afrobeat","anime","bossanova", "brazil","cantopop","forro","french","german","j-pop", "j-poprock","j-rock", "otacore", "visual kei"]

	genres = {'blues':blues, 'classical':classical,'country': country,'dance':dance,'electronic':electronic,'experimental':experimental,'folk':folk,'hiphop':hiphop,'indie':indie,'jazz':jazz,'metal':metal,'pop':pop,'punk':punk,'reggae':reggae,'religious':religious,'rnb':rnb,'rock':rock,'world' :world}

	for i in range(1000):
		runAll(genres)
	#genrefromSub(name,subgenres,genres)
	#add alternative- for each genre
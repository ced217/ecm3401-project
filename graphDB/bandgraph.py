
from relationships import Relationship
from Nodes.album import Album
from Nodes.musician import Musician
from Nodes.band import Band
from scrape import Scrape
from load import Load 
from driver import Driver
import requests

#use bin\neo4j start to run server

'''
This is where the run all will most likely be stored and 
where the driver object instance will be instanciated
and the db loading functions for the scraped data
'''

#db loading functions if necessary will pass data from the webscraper to the neo4j create functions



if __name__ == '__main__':
	



	print("\n Downloading Test")

	driver = Driver() # creating the driver object but other way round next time call create nodes in graph db
	scrape = Scrape()
	load = Load(driver)
	path = Relationship(driver)
	


	bandUrlList = scrape.downloadBand() 
	#musicianUrlList = scrape.downloadMusician()
	#albumUrlList = scrape.downloadAlbum()

	i = -1530 #back index from findIndex must always be negative- to ensure only last x are tekn
	httpbands = bandUrlList[i:] #place index on where ever restartC stops
	# ['http://www.bandtoband.com/band/26','http://www.bandtoband.com/band/stone-sour','http://www.bandtoband.com/search/?']
		#returns -  links between members and band. No albums displayed. 
		#		 - thus no links between members and albums 
		#		 - add bands to albums as properties
		#		 - get albums, url from list - visit each url scrape and connect it to the relvenat musician
		#		 - include recored by in the relationships between bands and artists

	 

	print ("\n Uploading...")

	#makegraph(urlList)
	
	
	
	for urlb in httpbands:
		valid = load.checkValid(urlb)
		#if url is valid then load the url otherwise continue 
		if valid:
			try:
				count = bandUrlList.index(urlb)
				total = len(bandUrlList)
				percent = count/total * 100
				print(urlb," urls downloaded:", count, "/",total,' ', percent,'%')
				load.loadBand(urlb)
			except (IndexError, requests.exceptions.TooManyRedirects):
				continue
		else :
			continue

	print ("!!!!!!!DONE!!!!")
	
	#load.createPaths() # creates relationships between muscians and band nodes based on wether there is a path connecting them
	#path.collaboratedWith()

	
	

	#all these constructors will be actually be alled in the dataloading class. 
	#The data loading class will then be called her using a run all function.
	
	#print("\n LOADING TEST")
	#band = Band(driver)
	#print(band.getMusicians())

	
	#print(album.getMusicians())
	 
	#driver.loadMusician("/artist/matthew-sanders")
	
	#driver.loadBand("/band/the-cranberries")
	

	#driver.loadAlbum("/band/nirvana-us-1/bleach")





	#--------------------------------------------------------------------------------------
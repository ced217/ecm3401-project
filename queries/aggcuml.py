import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import numpy as np

#Fig 1. The number of nodes  and edges in the full network when we aggregate from the earliest tstart = 1910

def dynamicAggregation():
	xyBn = countBandsDn() #xyB[0] = year, xyB[1] = number of nodes
	xyBr = countBandsDr()
	xyBn = orderCoords(xyBn) 
	xyBr = orderCoords(xyBr)
	
	
	
	bNodes = cumulative(xyBn[1])
	bRels = cumulative(xyBr[1])
	
	plotgraph(xyBn[0],bNodes,xyBr[0],bRels,'nodes','relationships','NodesEdgesBandNetwork','t','# nodes/edges (10^4)')


	xyMn = countMusiciansDn()
	xyMr = countMusiciansDr()
	xyMn = orderCoords(xyMn) 
	xyMr = orderCoords(xyMr)

	mNodes = cumulative(xyMn[1])
	mRels = cumulative(xyMr[1])
	plotgraph(xyMn[0],mNodes,xyMr[0],mRels,'nodes','relationships','NodesEdgesMusicianNetwork','t','# nodes/edges (10^5)')
	

def orderCoords(coOrds):
	coOrds = list(zip(coOrds[0],coOrds[1])) #zip the lists together as a list of tuples
	#print(coOrds)
	coOrds.sort(key=lambda x: x[0]) #sort the list of tuples acoording to the first element
	#print(coOrds)
	coOrds = list(zip(*coOrds)) #inverse zip (upzip) tuples and return two lists
	return coOrds

def cumulative(vs):
	vs = list(vs)
	vs = np.cumsum(vs) 
	vs = tuple(vs)
	return vs

'''
def toCsv(data,file):
	#dynamic csv file
	#static csv file

	#send result heading to file
	#send coloums to file
	#send result to file
'''



def countBandsDn():
	#USE slices to access all rows rather than iterating through json response, see spotipy?
	
	#Count dynamic growth of nodes at time interval
	print("sending query countBandsDN...") #counts the nodes at year intervals
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
		
	
	match = "Match (b:Band) WHere b.year <= \""
	ret = "\" RETURN b.year, count(b)"
	years = range(1910,2021,5)

	for y in years:
		y =  str(y)
		statement = match + y +ret
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	print('Status:', res.status_code)
	r = res.json()
	
	
	nrows = countRows(res)
	result =  r['results'][0]['data']
	x = [] #years
	y = []#number of nodes at given year
	for n in range(nrows):
		year = result[n]['row'][0]
		count = result[n]['row'][1]
		x.append(year)
		y.append(count)
	return(x,y)
		


	
	


def countBandsDr():	
	print("sending query countBandsDR...") #counts band relationships at year intervals

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	match = "Match  (b:Band)-[r:HaveCollaborated]-() WHere b.year <= \""
	ret = "\" RETURN b.year, count(r)"
	years = range(1910,2021,5)
	
	for y in years:
		y =  str(y)
		statement = match + y +ret
		#print(statement)
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	

	print('Status:',res.status_code)
	r = res.json()
	nrows = countRows(res)
	result =  r['results'][0]['data']
	x = [] #years
	y = []#number of nodes at given year
	for n in range(nrows):
		year = result[n]['row'][0]
		count = result[n]['row'][1]
		x.append(year)
		y.append(count)
	return(x,y)


def countMusiciansDn():
	print("sending query countMusiciansDn...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }	
	
	match =  "Match (m:Musician) WHere m.year <= \""
	ret = "\" RETURN m.year, count(m)"
	years = range(1910,2021,5)

	for y in years:
		y =  str(y)
		statement = match + y +ret
		#print(statement)
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}

	

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status:',res.status_code)
	r = res.json()
	nrows = countRows(res)
	result =  r['results'][0]['data']
	x = [] #years
	y = []#number of nodes at given year
	for n in range(nrows):
		year = result[n]['row'][0]
		count = result[n]['row'][1]
		x.append(year)
		y.append(count)
	return(x,y)

def countMusiciansDr():
	print("sending query countMusiciansDr...")
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	match = "Match (m:Musician)-[r:RecordedWith]-() WHere m.year <= \""
	ret = "\" RETURN m.year, count(r)"
	years = range(1910,2021,5)

	for y in years:
		y =  str(y)
		statement = match + y +ret
		#print(statement)
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status:',res.status_code)
	r = res.json()
	nrows = countRows(res)
	result =  r['results'][0]['data']
	x = [] #years
	y = []#number of nodes at given year
	for n in range(nrows):
		year = result[n]['row'][0]
		count = result[n]['row'][1]
		x.append(year)
		y.append(count)
	return(x,y)
	

def countRows(res):
	text = res.text
	n = text.count("row")
	return n


def plotgraph(xN,yN,xR,yR,nameN,nameR,titleG,titleX,titleY):
	#fix doggy outcome then create class, make lines connected along line (tather than scatter line)

	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
	
	trace0 = go.Scatter(
	    x= xN,
	    y=yN,
	    mode = 'lines+markers',
	    name = nameN
	
	
	)
	trace1 = go.Scatter(
	    x=xR,
 	   	y=yR,
 	   	mode = 'lines+markers',
	    name = nameR
	)
	data = [trace0, trace1]
	layout = dict(title = titleG,
	              xaxis = dict(
	              	title = titleX ,
	              	type='log',
	        		autorange=True),
	              yaxis = dict(
	              	title = titleY,
	              	type='log',
	        		autorange=True)
	              )
	fig = dict(data=data, layout=layout)
	
	py.plot(fig, filename = titleG)

if __name__ == '__main__':
	dynamicAggregation()

	#x = []
	#y = []
	'''
	result =  r['results'][0]['data']
	for x in range(nrows):
		year = result[n]['row'][0]
		count = result[n]['row'][1]
		print(year,count)
	'''
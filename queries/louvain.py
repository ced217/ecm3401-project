import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import collections as col
from gcc import getGCC


#replace allcommunity with community

def setcommunities(node,relationship,compID):
	print("sending query setting louvain...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }


	matchnode= "MATCH (x:"+node+" {component: "+compID +"})  RETURN id(x) as id"
	statement1 = "CALL algo.louvain('"+node+"', '"+relationship+"', {write:true, writeProperty:'community', weightProperty:'weight', defaultValue:1.0 })"
	statement2 = " YIELD nodes, communityCount, iterations, loadMillis, computeMillis, writeMillis;"
	

	statements = statement1+statement2


	data ={	
		"statements" : [ {
    		"statement" :  statements
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status: ', res.status_code)
	r = res.json()




def getcommunitys(label,compID):

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+label+" )" # place inide GCC
	statement2 = " RETURN distinct(x.community) as community, count(*) as size" #change component to community
	statement3 = " ORDER by size desc LIMIT 10;"

	#MATCH (x:Musician {component: "+compID +"}  ) RETURN distinct(xcommunity) ascommunity, count(*) as size 
	#ORDER by size desc LIMIT 5

	statement = statement1 + statement2 + statement3
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status: ', res.status_code)
	r = res.json()
	nrows = countRows(res)
	cs = [] #communityIDs
	css = [] #commmity sizes
	result =  r['results'][0]['data']
	
	for i in range(nrows):
		

		community = result[i]['row'][0] #ccommunity ID
		size = result[i]['row'][1] #community size
		#print(size, "nodes in gcc")
		#print(component, "component ID\n")
		cs.append(community) #list of communities
		css.append(size) #list of community sizes

	return(cs,css)



def communitysubgenres(label,cs):
	#cs is the list of the top five communities 
	#for now use components replace with communites 
	
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	mgs = [] #modal genres 
	mg5s =[] #top 5 modal genres
	for community in cs:
		comm = str(community)
		statement1 = "MATCH (x:"+label+" {community: "+comm+"})" #change component to community
		statement2 = " WHERE exists(x.community) and exists(x.genre) AND x.year>=1950 AND NOT x.genre = '[n]' AND NOT x.genre ='[]' "
		statement3 = " RETURN x.genre Limit 100"
		statement = statement1 + statement2 + statement3
		data ={	
		"statements" : [ {
    		"statement" : statement
    		
  			} ]
		}

		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status: ', res.status_code)
		r = res.json()
		#print(r)
		result =  r['results'][0]['data']
		nrows = countRows(res)
		
		gs = []
		for n in range(nrows): 
			genres = result[n]['row'][0]
			if genres: #if the list of genres is not empty
				gs.append(genres)
				#print(genres)
		#print(gs)
		mg = modalgenre(gs) #mg is the modal genre for that community
		mgs.append(mg)
		
	return(mgs)  #mgs to table top 5 communities and rank and size of community, mg5s top


def communitygenres(label,cs):
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	gs = [] #genres
	
	#ps = [20536,4,11,23091]#community test 
	for community in cs: #swap ps to cs 
		comm = str(community)
		statement1 = "MATCH (x:"+label+" {community: "+comm+"})" #change component to community
		statement2 = "WHERE exists(x.community) and exists(x.maingenre) AND x.year>=1950 AND NOT x.genre = '[n]' AND NOT x.genre ='[]' " #dont check empty genres 
		statement3 = "RETURN x.maingenre Limit 1"
		statement = statement1 + statement2 + statement3
		data ={	
		"statements" : [ {
    		"statement" : statement
    		
  			} ]
		}

		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status: ', res.status_code)
		r = res.json()
		result =  r['results'][0]['data']
		nrows = countRows(res)
		
		for n in range(nrows): 
			genres = result[n]['row'][0]
			#print(genres)
			if genres: #if the list of genres is not empty
				gs.append(genres)
	#print(gs)
	return gs

def modalgenre(genres):
	genreModes = col.Counter(genres).most_common(1)
	try:
		genre = genreModes[0]
		modal = genre[0] #modal genre, the genre with the highest mode
	except(IndexError):
		modal = 'N/A'
	return modal
def countRows(res):
	text = res.text
	n = text.count("row")
	return n




if __name__ == '__main__':

	b = 'Band'
	m = 'Musician'
	bR = 'HaveCollaborated'
	mR = 'RecordedWith'

	
	


	
	

	#setcommunities(b,bR,idB)
	#setcommunities(m,mR,idM)
	
	gccB = getGCC(b,'1')
	idB = str(gccB[0])
	print(idB)

	comSizeB = getcommunitys(b,idB) # Communites[1] and sizes[0]
	commsB = comSizeB[0] #community ids of top 5 communites
	comsizesB = comSizeB[1] #size of top 5 communites
	topsubGenresB = communitysubgenres(b,commsB)#genres of the top 5 communites
	topGenresB = communitygenres(b,commsB)

	#Largest 5 Band ccommunities
 
	print(commsB, "community ids of top 5 communites")
	print(comsizesB,"size of top 5 communites")
	print(topsubGenresB, "subgenres of the top 5 communites")
	print(topGenresB,"topgenres of the top 5 communities")

	#Largest 5 Muisician communities
	gccM = getGCC(m,'1')
	idM = str(gccM[0])
	print(idM)

	comSizeM = getcommunitys(m,idM)
	commsM = comSizeM[0]
	comsizesM = comSizeM[1]
	topsubGenresM = communitysubgenres(m,commsM)
	topGenresM = communitygenres(m,commsM)

	print(commsM, "community ids of top 5 communites")
	print(comsizesM,"size of top 5 communites")
	print(topsubGenresM, "subgenres of the top 5 communites")
	print(topGenresM,"topgenres of the top 5 communities")


'''
	comSizeB = getcommunitys(b) # tuple containing list of communities and thier sizes
	comSizeM = getcommunitys(m)
	
	communitiesB = comSizeB[0]
	communitiesM = comSizeM[0]

	sizesB = comSizeB[1]
	sizesM = comSizeM[1]

	#Top 5 communites and their size
	print(communitiesB,"list of top 5 largest band communites") #top 5 largest communities
	print(sizesB, size of top largest band communities)

	print(communitiesM,"list of top 5 largest musician communites") #top 5 largest communities
	print(sizesM, size of top 5 largest musician communities)

	#Get List of genres in each community
	#cgsB = communitysubgenres(communitiesB)
	#cgsM = communitysubgenres(communitiesM)
'''
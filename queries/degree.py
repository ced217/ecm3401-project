import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import collections as coll
from gcc import getGCC


def degreeHigh(node,relationship,compID):
	
	#returns the nodes with top 5 degrees at each year interval
	print("sending query Highest degrees over time")
	

	#limit = str(len(years))
	limit = str(10)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	#component = str(comp) 	

	statement1 = "MATCH (x:"+node+"  {component: "+compID +"})-[r:"+relationship+"]-() WHere x.year <=\"" 
	statement2 = "\" RETURN x.name AS node, count(*) AS degree"
	statement3 = " ORDER by degree desc LIMIT "+limit+";"

	years = range(1950,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}
		
		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		
		#print(r)
		result =  r['results'][0]['data']
		try:
			
			node = result[0]['row'][0]
			degree = result[0]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			node = 0
			degree = None

		xs.append(i)
		ys.append(degree)

	return(xs,ys) #years =x , size of communities = y




def degreeAv(node,relationship,compID):
	print("sending query get average degrees over time...")
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	#component = str(comp)

	statement1 ="MATCH (x:"+node+"  {component: "+compID +"})-[r:"+relationship+"]-() Where x.year <= \""
	statement2 = "\" WITH x.name AS node, count(*) AS degree" 
	statement3 = " RETURN avg(degree) as av_degree LIMIT 1"

	years = range(1950,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}
		
		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		
		#print(r)
		result =  r['results'][0]['data']
		try:
			
			#node = result[0]['row'][0]
			degree = result[0]['row'][0]

		except(IndexError):
			"No value for size or component at time t"
			node = 0
			degree = None

		xs.append(i)
		ys.append(degree)

	return(xs,ys) #years =x , size of communities = y




def degrees(node,relationship,year,compID):
	#the year is passed in before
	
	ds= []
	#component = str(comp)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+node+"  {component: "+compID +"})-[r:"+relationship+"]-() WHERE   x.year <=\""+str(year)+"\" "  
	statement2 = " RETURN x.name AS node, count(*) AS degree"
	#statement3 = " ORDER by degree desc"

	statement = statement1 + statement2 
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	#print('Status: ', res.status_code)
	r = res.json()
	
	#print(r)
	result =  r['results'][0]['data']
	#print(result)
	nrows = countRows(res)
	for n in range(nrows):
		try:
			#size =  result[0]['row'][1]
			#componentL = result[0]['row'][0]
			deg = result[n]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			deg = 0

		ds.append(deg)
	return(ds)
	#print(ds)

def countRows(res):
	text = res.text
	n = text.count("row")
	return n

def degreeDist(node,relationship,compID):
	print("sending query degree distribution "+node+" ")

#degreeD('Band','HaveCollaborated')
#degreeHigh('Band','HaveCollaborated')#
	xs = [] #degrees (k)
	names = []
	#ys = [] #proportion of nodes with degree p(k)
	#ds = degrees(node,relationship,year)
	#print(ds)
	years = range(1950,2021,5)
	for year in years:
		ds = degrees(node,relationship,year,compID)
		xs.append(ds)
		names.append(year)
		#print(year)
	plotHist(xs,names,'Degree distribution'+node+'graph','k','p(k)',node)
	#return xs

def highestDegreeStat(node,relationship,prop,compID):
	#Lists the highest degrees of all time for bands and musicians
	limit = str(10)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	ns = [] #nodes
	ds = [] #degrees

	statement1 = "MATCH (x:"+node+"  {component: "+compID +"} )-[r:"+relationship+"]-() WHERE x.year <= 1950 " 
	statement2 = " RETURN x."+prop+" AS node, count(*) AS degree"
	statement3 = " ORDER by degree desc LIMIT "+limit+";"

	statement = statement1 + statement2 + statement3
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	r = res.json()
		
	print(r)
	result =  r['results'][0]['data']
	
	for n in range(int(limit)):
		node = result[n]['row'][0]
		degree = result[n]['row'][1]
		ns.append(node)
		ds.append(degree)
	return (ds,ns)
	


def plotHist(xs,names,titleG,titleX,titleY,node): #titleX = k titleY = p(k)
	
	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
		
#np.random.randn(500)
	data = []
	length = len(names)
	for n in range(length):
		
		try:
			x = xs[n]
			#print(x)
			name = names[n]
			#print(name)
		except(IndexError):
			x = [0]
			name = 'Name Error and name is blank'

		trace = go.Histogram(
 	 	  	x = x,
 	 	  	
 	 	  	histnorm='probability',
 	   		name=name
 	   		
		)

		data.append(trace)
	

	layout = go.Layout(
		title = titleG,
		xaxis=dict(
      	#	type='log',
        #	autorange=True
        	title =titleX
    	),
    	yaxis=dict(
        	type='log',
        	autorange=True,
        	title = titleY
    	),
    	barmode='stack'

	)
	fig = go.Figure(data=data, layout=layout)

	py.plot(fig, filename=titleG)
	


def plotgraph(xB,yB,xM,yM,nameB,nameM,titleG,titleX,titleY):
	#fix doggy outcome then create class, make lines connected along line (tather than scatter line)

	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
	
	trace0 = go.Scatter(
	    x= xB,
	    y=yB,
	    mode = 'lines+markers',
	    name = nameB
	
	
	)
	trace1 = go.Scatter(
	    x=xM,
 	   	y=yM,
 	   	mode = 'lines+markers',
	    name = nameM
	)
	data = [trace0, trace1]
	layout = dict(title = titleG,
	              xaxis = dict(
	              	title = titleX ,
	              	type='log',
	        		autorange=True),
	              yaxis = dict(
	              	title = titleY,
	              	type='log',
	        		autorange=True)
	              )
	fig = dict(data=data, layout=layout)
	
	py.plot(fig, filename = titleG)




#print(xsB)

#dsM = degreeDist('Musician', 'RecordedWith')
if __name__ == '__main__':
	
	b = 'Band'
	bR = 'HaveCollaborated'
	
	m = 'Musician'
	mR = 'RecordedWith'
	titleX = 'year(t)'
	titleY = 'degree (k)'

	gccB = getGCC(b,str(1)) #limit 1 row 0returns the highest GCC 
	gccM =getGCC(m,str(1))
	
	#gcc[0] => ID , gcc[1] => size
	
	idB = str(gccB[0])
	idM = str(gccM[0])
	print(idB)
	print(idM)
	'''

	#Highest Degree over time
	cB = degreeHigh(b,bR,idB)
	cM = degreeHigh(m,mR,idM)
	plotgraph(cB[0],cB[1],cM[0],cM[1],b,m,'Highest degrees over time',titleX,titleY)
	
	#Average Degree over time
	cBav = degreeAv(b,bR,idB)
	cMav = degreeAv(m,mR,idM)
	plotgraph(cBav[0],cBav[1],cMav[0],cMav[1],b,m,'Average degrees  over time', titleX,titleY)
	'''
	#Musicians and Bands with highest degrees ranked from highest.
	tabB = highestDegreeStat(b,bR,'bandName',idB)
	tabM = highestDegreeStat(m,mR,'fullName',idM)
	print('Bands with highest degree:', tabB)
	print('Musicians with highest degree', tabM)

	#Degree Distributions
	#degreeDist(b,bR,idB)
	#degreeDist(m,mR,idM)


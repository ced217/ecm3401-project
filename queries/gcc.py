import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

#Number of Nodes in the Larges componenet divided by total number of nodes aggregated in tstart = 50
def setComponents(node,rel):
	print("sending query setting "+node+" Components...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "CALL algo.unionFind('"+ node+"', '"+rel+"', {write:true, partitionProperty:'component'})" 
	statement2 = "YIELD nodes, setCount, loadMillis, computeMillis, writeMillis;"

	statement = statement1 + statement2
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status: ', res.status_code)
	r = res.json()
	
	print(r)
	#print(r['results'][0]['data'][0]['row'][0], "Total Number of Band nodes")
	#print(r['results'][1]['data'][0]['row'][0], "Total Number of Band relationships")

def getGCC(node,limit):
	print("sending query get largest "+node+" Components...")


	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+node+")" 
	statement2 = " RETURN distinct(x.component) as component, count(*) as size"
	statement3 = " ORDER by size desc LIMIT "+limit+";"

	statement = statement1 + statement2 + statement3
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status: ', res.status_code)
	r = res.json()
	result =  r['results'][0]['data']
	
	component = result[0]['row'][0]
	size = result[0]['row'][1]
	#print(size, "nodes in gcc")
	#print(component, "component ID\n")
	return(component,size)

	

def getGCCD(node,compS):
	print("sending query size get largest "+node+" Components over time...")
	

	#limit = str(len(years))
	limit = str(5)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+node+")  WHere x.year <= \"" 
	statement2 = "\" RETURN distinct(x.component) as component, count(*) as size"
	statement3 = " ORDER by size desc LIMIT "+limit+";"

	years = range(1910,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}
		
		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		
		#print(r)
		result =  r['results'][0]['data']
		#print(result)
		try:
			#size =  result[0]['row'][1]
			componentL = result[0]['row'][0]
			size = result[0]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			size = 0
			componentL = None
		#print("Size",size)
		#print(i)
		
		xs.append(i)
		ys.append(size)
		cs.append(componentL)
	#print('years in time t', xs)
	#print('size of components', ys)
	
	ys = [y/compS for y in ys]
	#print('proportion of nodes in component', ys)
	#print('largest component at time t', cs)

	return(xs,ys) #years =x , size of communities = y


def get2ndGCCD(node,compS):
	print("sending query get size second largest"+node+" Components over time...")

	#limit = str(len(years))
	limit = str(5)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+node+")  WHere x.year <= \"" 
	statement2 = "\" RETURN distinct(x.component) as component, count(*) as size"
	statement3 = " ORDER by size desc LIMIT "+limit+";"

	years = range(1910,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}
		
		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		
		#print(r)
		result =  r['results'][0]['data']
		#print(result)

	#componentL = result[1]['row'][0] #this gets the second result value
	#size = result[1]['row'][1] #this gets the second size value

		try:
			#size =  result[0]['row'][1]
			componentL = result[1]['row'][0]
			size = result[1]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			size = 0
			componentL = None
		#print("Size",size)
		#print(i)
		
		xs.append(i)
		ys.append(size)
		cs.append(componentL)
	#print('years in time t', xs)
	#print('size of components', ys)
	
	ys = [y/compS for y in ys]
	#print('proportion of nodes in component', ys)
	#print('largest component at time t', cs)

	return(xs,ys) #years =x , size of communities = y


def getavGCCD(node,compS):
	print("sending query get average size of "+node+" Components over time...")
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }

	statement1 = "MATCH (x:"+node+")  WHere x.year <= \"" 
	statement2 = "\" WITH distinct(x.component) as component, count(*) as size"
	statement3 = " RETURN avg(size) as av_size LIMIT 1;"

	years = range(1910,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}
		
		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		
		#print(r)
		result =  r['results'][0]['data']
		

		try:
			#size =  result[0]['row'][1]
			
			size = result[0]['row'][0]
			if size == None:
				size = 0

		except(IndexError):
			"No value for size or component at time t"
			size = 0
			#componentL = None
		#print("Size",size)
		#print(i)
		
		xs.append(i)
		ys.append(size)
		#cs.append(componentL)
	#print('years in time t', xs)
	#print('size of components', ys)
	
	ys = [y/compS for y in ys]
	#print('proportion of nodes in component', ys)
	#print('largest component at time t', cs)

	return(xs,ys) #years =x , size of communities = y


def countRows(res):
	text = res.text
	n = text.count("row")
	return n

def plotgraph(xB,yB,xM,yM,nameB,nameM,titleG,titleX,titleY):
	#fix doggy outcome then create class, make lines connected along line (tather than scatter line)

	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
	
	trace0 = go.Scatter(
	    x= xB,
	    y=yB,
	    mode = 'lines+markers',
	    name = nameB
	
	
	)
	trace1 = go.Scatter(
	    x=xM,
 	   	y=yM,
 	   	mode = 'lines+markers',
	    name = nameM
	)
	data = [trace0, trace1]
	layout = dict(title = titleG,
	              xaxis = dict(
	              	title = titleX ,
	              	type='log',
	        		autorange=True),
	              yaxis = dict(
	              	title = titleY,
	              	type='log',
	        		autorange=True)
	              )
	fig = dict(data=data, layout=layout)
	
	py.plot(fig, filename = titleG)

if __name__ == '__main__':
	setComponents('Band','HaveCollaborated') #sets the component property for all the bands
	setComponents('Musician', 'RecordedWith')
	
	B = 'Band'
	M = 'Musician'

	gccB = getGCC(B,'1') #limit 1 row 0returns the highest GCC 
	gccM = getGCC(M,'1')
	
	#gcc[0] => ID , gcc[1] => size
	
	idB = gccB[0]
	idM = gccM[0]

	sizeB = gccB[1]
	sizeM = gccM[1]
	print(sizeB, "nodes Band in gcc")
	print(idB, " largestcomponent in Band ID\n")
	
	print(sizeM, "nodes Musician in gcc")
	print(idM, "largest component in Musician ID\n")
	
	compSB = sizeB
	compSM = sizeM
	x_axis = 'years (t)'
	y_axis = 'component size p(n)'
	
	
	# plot graph showing size of  largest components  over time
	coOrdsB = getGCCD(B,compSB)
	coOrdsM = getGCCD(M,compSM)
	plotgraph( coOrdsB[0],coOrdsB[1],coOrdsM[0],coOrdsM[1],B,M,'(a)',x_axis,y_axis)
	

	# plot graph showing size of 2nd largest components over time
	cB2 = get2ndGCCD(B,compSB)
	cM2 = get2ndGCCD(M,compSM)
	plotgraph( cB2[0],cB2[1],cM2[0],cM2[1],B,M,'(b)',x_axis,y_axis)

	# plot graph showing size of mean components over time
	cBav =  getavGCCD(B,compSB)
	cMav =  getavGCCD(M,compSM)
	plotgraph( cBav[0],cBav[1],cMav[0],cMav[1],B,M,'(c)',x_axis,y_axis)

import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

def diameter():
	print("sending query shortestPaths...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	component = 
	'''
	statement1 = "CALL algo.allShortestPaths.stream('weight',{nodeQuery:'Band',defaultValue:1.0}) "
	statement2 = "YIELD sourceNodeId, targetNodeId, distance "
	statement3 = "WITH sourceNodeId, targetNodeId, distance "
	statement4 = "WHERE algo.isFinite(distance) = true "
	statement5 = "RETURN sourceNodeId, targetNodeId, max(distance) LIMIT 20"
	statements = statement1+statement2+statement3+statement4+statement5

	'''

	statement1 = "MATCH p=shortestPath((a:Band)-[:HaveCollaborated*]-(b:Band)) "
	statement2 = "WHERE id(a) > id(b) AND a.component = "+component+" and b.component = "+component+"  "
	statement3 = "WITH length(p) AS len, p "
	statement4 = "ORDER BY len DESC LIMIT 4 "
	#statement5 = "RETURN len, extract(x IN nodes(p) | x) AS path"


	statements =  statement1+statement2+statement3+statement4

	data ={	
		"statements" : [ {
    		"statement" :  statements
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status: ', res.status_code)
	r = res.json()
	#print(r['results'][0]['data'][0], "data")
	print(r, "results")
	#print(r['results'][0]['data'][0]['row'][0], "sourceNodeId")
	#return lis


#distances = []

#averageshortestPath()

CALL algo.allShortestPaths.stream('cost', {
nodeQuery:'MATCH (x:Loc) RETURN id(n) as id where n.component = comp', maxComponent = 
relationshipQuery:'MATCH (x:Loc)-[r]-(y:Loc) RETURN id(n) as source, id(p) as target, r.cost as weight',
graph:'cypher', defaultValue:1.0})
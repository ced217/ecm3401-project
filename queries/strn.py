import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

def getGCC(node):
	print("sending query get largest "+node+" Components...")


	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+node+" )" 
	statement2 = " RETURN distinct(x.component) as component, count(*) as size"
	statement3 = " ORDER by size desc LIMIT 1"

	statement = statement1 + statement2 + statement3
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	print('Status: ', res.status_code)
	r = res.json()
	result =  r['results'][0]['data']
	
	component = result[0]['row'][0]
	size = result[0]['row'][1]
	#print(size, "nodes in gcc")
	#print(component, "component ID\n")
	return(component,size)

def strengths(node,relationship,year,compID):
	ss= []

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "MATCH (x:"+node+" {component: "+compID +"})-[r:"+relationship+"]-() WHERE x.year <=" +str(year)   
	statement2 = " RETURN x.name AS node, sum(r.weight) AS strength"
	#statement3 = " ORDER by degree desc"

	statement = statement1 + statement2 
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	#print('Status: ', res.status_code)
	r = res.json()
	
	#print(r)
	result =  r['results'][0]['data']
	nrows = countRows(res)
	for n in range(nrows):
		try:
			#size =  result[0]['row'][1]
			#componentL = result[0]['row'][0]
			strn = result[n]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			strn = 0

		ss.append(strn)
	return(ss)

def countRows(res):
	text = res.text
	n = text.count("row")
	return n

def strengthDist(node,relationship,compID):
	xs = [] #degrees (k)
	names = []
	
	years = range(1950,2021,5)
	for year in years:
		ss = strengths(node,relationship,year,compID)
		xs.append(ss)
		names.append(year)
	#print(xs,names)
	plotHist(xs,names,'Strength distribution '+node+' graph','s','p(s)',node)

def plotHist(xs,names,titleG,titleX,titleY,node): #titleX = k titleY = p(k)
	
	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
		
#np.random.randn(500)
	data = []
	length = len(names)
	for n in range(length):
		
		try:
			x = xs[n]
			name = names[n]
		except(IndexError):
			x = [0]
			name = 'Name Error and name is blank'

		trace = go.Histogram(
 	 	  	x = x,
 	 	  	
 	 	  	histnorm='probability',
 	   		name=name
 	   		
		)

		data.append(trace)
		#trace1 = go.Histogram(
 		# 	x = xs[7],
 		#	histnorm='probability',
 	   	#	name=names[7]
		#)

	#data = [trace0,trace1 ]

	
	layout = go.Layout(
		title = titleG,
		xaxis=dict(
      	#	type='log',
        #	autorange=True
        	title =titleX
    	),
    	yaxis=dict(
        	type='log',
        	autorange=True,
        	title = titleY
    	),
    	barmode='stack'

	)
	fig = go.Figure(data=data, layout=layout)

	py.plot(fig, filename=titleG)

if __name__ == '__main__':
	#setComponents('Band','HaveCollaborated') #sets the component property for all the bands
	#setComponents('Musician', 'RecordedWith')
	
	B = 'Band'
	M = 'Musician'

	gccB = getGCC(B) #limit 1 row 0returns the highest GCC 
	gccM =getGCC(M)
	
	#gcc[0] => ID , gcc[1] => size
	
	idB = gccB[0]
	idM = gccM[0]
	print(idB)
	print(idM)

	sizeB = gccB[1]
	sizeM = gccM[1]


	#strengthDist(B,'HaveCollaborated', str(idB))
	strengthDist(M,'RecordedWith', str(idM))

import requests
import json
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from gcc import getGCC
#CALL algo.betweenness(
#Band,
#CollaboratedWith,
#{ write: true, writeProperty: 'betweenness'});

#Match (x:Band) Where exists(x.betweenness) Return x.name as node, x.betweenness as betweenness order by betweenness desc Limit 5



def setBetweeness(node,relationship,compID):
	print("sending query setting "+node+" betweenness...")

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	#matchnode = "'MATCH (x:"+node+" {component: "+compID +"})  RETURN id(x) as id'"
	#matchrel = "'MATCH (x1:"+node+" {component: "+compID +"})-[r:"+relationship+"]-(x2) RETURN id(x1) as source, id(x2) as target, r.weight as weight'"
	
	statement1 = "CALL algo.betweenness('"+node+"','"+relationship+"',{write: true, writeProperty: 'betweenness'})" 
	statement2 = "YIELD nodes, minCentrality, maxCentrality, sumCentrality, loadMillis, computeMillis, writeMillis;"

	statement = statement1 + statement2
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	#print('Status: ', res.status_code)
	r = res.json()
	print(r)


	

def btwnHigh(node,compID):
	
	#returns the nodes with top 5 degrees at each year interval
	print("sending query size get  "+node+" with the highest betweeness over time...")
	

	#limit = str(len(years))
	limit = str(5)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "Match (x:"+node+"  {component: "+compID +"}) Where exists(x.betweenness)  and x.year <=" #flip back around
	statement2 = " Return x.name as node, x.betweenness as betweenness"
	statement3 = " order by betweenness desc LIMIT "+limit+";"

	years = range(1950,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}

		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		#print(r)
		
		result =  r['results'][0]['data']
		try:
			
			node = result[0]['row'][0]
			btwn = result[0]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			node = 0
			btwn = None

		xs.append(i)
		ys.append(btwn)

	return(xs,ys) #years =x , size of communities = y


def btwnAv(node,compID):
	
	print("sending query get average betweeness of "+node+"  over time...")
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server

	statement1 = "Match (x:"+node+" {component: "+compID +"}) Where exists(x.betweenness)  and x.year <=" 
	statement2 = " With x.name as node, x.betweenness as betweenness"
	statement3 = " RETURN avg(betweenness) as av_btwn LIMIT 1"
	
	years = range(1950,2021,5)
	xs = [] #x is the years
	ys = [] # the number of components in the component the biggest component
	cs = [] #the list of the components
	for i in years:
		y =  str(i)
		statement = statement1 + y + statement2 + statement3
	
		data ={	
			"statements" : [ {
    			"statement" : statement
  			} ]
		}
		
		data = json.dumps(data)
		res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
		#print('Status:',res.status_code)
		r = res.json()
		print(r)
		
		result =  r['results'][0]['data']
		try:
			
			#node = result[0]['row'][0]
			btwn = result[0]['row'][0]

		except(IndexError):
			"No value for size or component at time t"
			node = 0
			btwn = None

		xs.append(i)
		ys.append(btwn)

	return(xs,ys) #years =x , size of communities = y


def btwns(node,year,compID):
	bs= []

	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	
	statement1 = "Match (x:"+node+" {component: "+compID +"}) Where exists(x.betweenness) and  x.year <="+str(year)+" "  
	statement2 = " Return x.name as node, x.betweenness as betweenness"
	#statement3 = " ORDER by degree desc"

	statement = statement1 + statement2 
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	#print('Status: ', res.status_code)
	r = res.json()
	
	#print(r)
	result =  r['results'][0]['data']
	nrows = countRows(res)
	for n in range(nrows):
		try:
			#size =  result[0]['row'][1]
			#componentL = result[0]['row'][0]
			btwn = result[n]['row'][1]

		except(IndexError):
			"No value for size or component at time t"
			btwn = 0

		bs.append(btwn)
		#print(bs)
	return(bs)
#setBetweeness('Band','HaveCollaborated')
#setBetweeness('Musician','RecordedWith')
def countRows(res):
	text = res.text
	n = text.count("row")
	return n

def btwnDist(node,compID):
	xs = [] #degrees (k)
	names = []
	#ys = [] #proportion of nodes with degree p(k)
	#ds = degrees(node,relationship,year)
	#print(ds)
	years = range(1950,2021,5)
	for year in years:
		bs = btwns(node,year,compID)
		xs.append(bs)
		names.append(year)
	plotHist(xs,names,'betweenness distribution'+node+'graph','b','p(b)',node)

def highestBtwnStat(node, prop,compID):
	#Lists the highest degrees of all time for bands and musicians
	limit = str(10)
	user = "neo4j"
	password = "chaoticImp291295" #Authenticate to access the server
	headers = { "Content-Type": "application/json; charset=UTF-8",'X-Stream' : 'true' }
	ns = [] #nodes
	bs = [] #degrees

	statement1 = "Match (x:"+node+" {component: "+compID +"}) Where exists(x.betweenness) AND x.year >= 1950 " 
	statement2 = " Return x."+prop+" as node, x.betweenness as betweenness"
	statement3 = " ORDER by betweenness desc LIMIT "+limit+";"

	statement = statement1 + statement2 + statement3
	data ={	
		"statements" : [ {
    		"statement" : statement
    		
  		} ]
	}

	data = json.dumps(data)
	res = requests.post("http://localhost:7474/db/data/transaction/commit", auth=(user, password), data=data)
	
	r = res.json()
		
	
	result =  r['results'][0]['data']
	#print(result)
	for n in range(int(limit)):
		node = result[n]['row'][0]
		btwn = result[n]['row'][1]
		ns.append(node)
		bs.append(btwn)
	return (bs,ns)
	


def plotHist(xs,names,titleG,titleX,titleY,node): #titleX = k titleY = p(k)
	
	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
		
#np.random.randn(500)
	data = []
	length = len(names)
	for n in range(length):
		
		try:
			x = xs[n]
			name = names[n]
		except(IndexError):
			x = [0]
			name = 'Name Error and name is blank'

		trace = go.Histogram(
 	 	  	x = x,
 	 	  	
 	 	  	histnorm='probability',
 	   		name=name
 	   		
		)

		data.append(trace)
	
	layout = go.Layout(
		title = titleG,
		xaxis=dict(
      	#	type='log',
        #	autorange=True
        	title =titleX
    	),
    	yaxis=dict(
        	type='log',
        	autorange=True,
        	title = titleY
    	),
    	barmode='stack'

	)
	fig = go.Figure(data=data, layout=layout)

	py.plot(fig, filename=''+node+'betweenness histogram')

def plotgraph(xB,yB,xM,yM,nameB,nameM,titleG,titleX,titleY):
	#fix doggy outcome then create class, make lines connected along line (tather than scatter line)

	key = '0SY41cK9dnOU1DJhdsl8'
	plotly.tools.set_credentials_file(username='ced217', api_key=key)
	
	trace0 = go.Scatter(
	    x= xB,
	    y=yB,
	    mode = 'lines+markers',
	    name = nameB
	
	
	)
	trace1 = go.Scatter(
	    x=xM,
 	   	y=yM,
 	   	mode = 'lines+markers',
	    name = nameM
	)
	data = [trace0, trace1]
	layout = dict(title = titleG,
	              xaxis = dict(
	              	title = titleX ,
	              	type='log',
	        		autorange=True),
	              yaxis = dict(
	              	title = titleY,
	              	type='log',
	        		autorange=True)
	              )
	fig = dict(data=data, layout=layout)
	
	py.plot(fig, filename = titleG)

if __name__ == '__main__':
	

	b = 'Band'
	bR = 'HaveCollaborated'
	
	m = 'Musician'
	mR = 'RecordedWith'
	titleX = 'year(t)'
	titleY = 'betweeness (b)'

	gccB = getGCC(b,str(1)) #limit 1 row 0returns the highest GCC 
	gccM =getGCC(m,str(1))
	
	#gcc[0] => ID , gcc[1] => size
	
	idB = str(gccB[0])
	idM = str(gccM[0])
	print(idB)
	print(idM)
	#setBetweeness(b,bR,idB)
	
	#setBetweeness(m,mR,idM)
	'''
	#Highest betweeness over time
	bB = btwnHigh(b,idB) #/10^5 #just divisions
	bM = btwnHigh(m,idM) 
	plotgraph(bB[0],bB[1],bM[0],bM[1],b,m,'Highest Betweeness overtime',titleX,titleY)


	#Average betweeness over time
	baB = btwnAv(b,idB) 
	print('list of average band betweeness',baB)
	baM = btwnAv(m,idM) 
	print('list of average musician betweeness',baM)
	#titleY = 'betweeness (b) 10^7'
	plotgraph(baB[0],baB[1],baM[0],baM[1],b,m,'Average Betweeness overtime',titleX,titleY)

	'''
	#Musciians and bands with highest betweeness ranked from hgihest
	tabB = highestBtwnStat(b,'bandName',idB)
	tabM = highestBtwnStat(m,'fullName',idM)
	print('Bands with highest betweenness:', tabB)
	print('Musicians with highest betweenness', tabM)

	#Betweeness Distributions
	#btwnDist(b,idB) 
	#btwnDist(m,idM) 
	